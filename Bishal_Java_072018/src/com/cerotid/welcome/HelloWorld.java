package com.cerotid.welcome;

public class HelloWorld {

	public static void main(String [] args) {
		System.out.println("Hello, World!");
		
		runMethodOne();
	}

	private static void runMethodOne() {
		System.out.println("Hey, I am from Method 1");
	}
}
